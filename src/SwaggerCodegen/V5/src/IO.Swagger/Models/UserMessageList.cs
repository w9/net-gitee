/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.28
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 列出授权用户的所有私信
    /// </summary>
    [DataContract]
    public partial class UserMessageList : IEquatable<UserMessageList>
    { 
        /// <summary>
        /// Gets or Sets TotalCount
        /// </summary>
        [DataMember(Name="total_count")]
        public int? TotalCount { get; set; }

        /// <summary>
        /// 私信列表
        /// </summary>
        /// <value>私信列表</value>
        [DataMember(Name="list")]
        public List<UserMessage> List { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class UserMessageList {\n");
            sb.Append("  TotalCount: ").Append(TotalCount).Append("\n");
            sb.Append("  List: ").Append(List).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((UserMessageList)obj);
        }

        /// <summary>
        /// Returns true if UserMessageList instances are equal
        /// </summary>
        /// <param name="other">Instance of UserMessageList to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(UserMessageList other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    TotalCount == other.TotalCount ||
                    TotalCount != null &&
                    TotalCount.Equals(other.TotalCount)
                ) && 
                (
                    List == other.List ||
                    List != null &&
                    List.SequenceEqual(other.List)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (TotalCount != null)
                    hashCode = hashCode * 59 + TotalCount.GetHashCode();
                    if (List != null)
                    hashCode = hashCode * 59 + List.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(UserMessageList left, UserMessageList right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(UserMessageList left, UserMessageList right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
