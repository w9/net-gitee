/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.28
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 仓库的所有提交
    /// </summary>
    [DataContract]
    public partial class RepoCommit : IEquatable<RepoCommit>
    { 
        /// <summary>
        /// Gets or Sets Url
        /// </summary>
        [DataMember(Name="url")]
        public string Url { get; set; }

        /// <summary>
        /// Gets or Sets Sha
        /// </summary>
        [DataMember(Name="sha")]
        public string Sha { get; set; }

        /// <summary>
        /// Gets or Sets HtmlUrl
        /// </summary>
        [DataMember(Name="html_url")]
        public string HtmlUrl { get; set; }

        /// <summary>
        /// Gets or Sets CommentsUrl
        /// </summary>
        [DataMember(Name="comments_url")]
        public string CommentsUrl { get; set; }

        /// <summary>
        /// Gets or Sets Commit
        /// </summary>
        [DataMember(Name="commit")]
        public string Commit { get; set; }

        /// <summary>
        /// Gets or Sets Author
        /// </summary>
        [DataMember(Name="author")]
        public string Author { get; set; }

        /// <summary>
        /// Gets or Sets Committer
        /// </summary>
        [DataMember(Name="committer")]
        public string Committer { get; set; }

        /// <summary>
        /// Gets or Sets Parents
        /// </summary>
        [DataMember(Name="parents")]
        public string Parents { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class RepoCommit {\n");
            sb.Append("  Url: ").Append(Url).Append("\n");
            sb.Append("  Sha: ").Append(Sha).Append("\n");
            sb.Append("  HtmlUrl: ").Append(HtmlUrl).Append("\n");
            sb.Append("  CommentsUrl: ").Append(CommentsUrl).Append("\n");
            sb.Append("  Commit: ").Append(Commit).Append("\n");
            sb.Append("  Author: ").Append(Author).Append("\n");
            sb.Append("  Committer: ").Append(Committer).Append("\n");
            sb.Append("  Parents: ").Append(Parents).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((RepoCommit)obj);
        }

        /// <summary>
        /// Returns true if RepoCommit instances are equal
        /// </summary>
        /// <param name="other">Instance of RepoCommit to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(RepoCommit other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    Url == other.Url ||
                    Url != null &&
                    Url.Equals(other.Url)
                ) && 
                (
                    Sha == other.Sha ||
                    Sha != null &&
                    Sha.Equals(other.Sha)
                ) && 
                (
                    HtmlUrl == other.HtmlUrl ||
                    HtmlUrl != null &&
                    HtmlUrl.Equals(other.HtmlUrl)
                ) && 
                (
                    CommentsUrl == other.CommentsUrl ||
                    CommentsUrl != null &&
                    CommentsUrl.Equals(other.CommentsUrl)
                ) && 
                (
                    Commit == other.Commit ||
                    Commit != null &&
                    Commit.Equals(other.Commit)
                ) && 
                (
                    Author == other.Author ||
                    Author != null &&
                    Author.Equals(other.Author)
                ) && 
                (
                    Committer == other.Committer ||
                    Committer != null &&
                    Committer.Equals(other.Committer)
                ) && 
                (
                    Parents == other.Parents ||
                    Parents != null &&
                    Parents.Equals(other.Parents)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (Url != null)
                    hashCode = hashCode * 59 + Url.GetHashCode();
                    if (Sha != null)
                    hashCode = hashCode * 59 + Sha.GetHashCode();
                    if (HtmlUrl != null)
                    hashCode = hashCode * 59 + HtmlUrl.GetHashCode();
                    if (CommentsUrl != null)
                    hashCode = hashCode * 59 + CommentsUrl.GetHashCode();
                    if (Commit != null)
                    hashCode = hashCode * 59 + Commit.GetHashCode();
                    if (Author != null)
                    hashCode = hashCode * 59 + Author.GetHashCode();
                    if (Committer != null)
                    hashCode = hashCode * 59 + Committer.GetHashCode();
                    if (Parents != null)
                    hashCode = hashCode * 59 + Parents.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(RepoCommit left, RepoCommit right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(RepoCommit left, RepoCommit right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
