/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 5.4.28
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 更新一个仓库WebHook
    /// </summary>
    [DataContract]
    public partial class Hook : IEquatable<Hook>
    { 
        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [DataMember(Name="id")]
        public string Id { get; set; }

        /// <summary>
        /// Gets or Sets Url
        /// </summary>
        [DataMember(Name="url")]
        public string Url { get; set; }

        /// <summary>
        /// Gets or Sets CreatedAt
        /// </summary>
        [DataMember(Name="created_at")]
        public string CreatedAt { get; set; }

        /// <summary>
        /// Gets or Sets Password
        /// </summary>
        [DataMember(Name="password")]
        public string Password { get; set; }

        /// <summary>
        /// Gets or Sets ProjectId
        /// </summary>
        [DataMember(Name="project_id")]
        public string ProjectId { get; set; }

        /// <summary>
        /// Gets or Sets Result
        /// </summary>
        [DataMember(Name="result")]
        public string Result { get; set; }

        /// <summary>
        /// Gets or Sets ResultCode
        /// </summary>
        [DataMember(Name="result_code")]
        public string ResultCode { get; set; }

        /// <summary>
        /// Gets or Sets PushEvents
        /// </summary>
        [DataMember(Name="push_events")]
        public string PushEvents { get; set; }

        /// <summary>
        /// Gets or Sets TagPushEvents
        /// </summary>
        [DataMember(Name="tag_push_events")]
        public string TagPushEvents { get; set; }

        /// <summary>
        /// Gets or Sets IssuesEvents
        /// </summary>
        [DataMember(Name="issues_events")]
        public string IssuesEvents { get; set; }

        /// <summary>
        /// Gets or Sets NoteEvents
        /// </summary>
        [DataMember(Name="note_events")]
        public string NoteEvents { get; set; }

        /// <summary>
        /// Gets or Sets MergeRequestsEvents
        /// </summary>
        [DataMember(Name="merge_requests_events")]
        public string MergeRequestsEvents { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Hook {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  Url: ").Append(Url).Append("\n");
            sb.Append("  CreatedAt: ").Append(CreatedAt).Append("\n");
            sb.Append("  Password: ").Append(Password).Append("\n");
            sb.Append("  ProjectId: ").Append(ProjectId).Append("\n");
            sb.Append("  Result: ").Append(Result).Append("\n");
            sb.Append("  ResultCode: ").Append(ResultCode).Append("\n");
            sb.Append("  PushEvents: ").Append(PushEvents).Append("\n");
            sb.Append("  TagPushEvents: ").Append(TagPushEvents).Append("\n");
            sb.Append("  IssuesEvents: ").Append(IssuesEvents).Append("\n");
            sb.Append("  NoteEvents: ").Append(NoteEvents).Append("\n");
            sb.Append("  MergeRequestsEvents: ").Append(MergeRequestsEvents).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Hook)obj);
        }

        /// <summary>
        /// Returns true if Hook instances are equal
        /// </summary>
        /// <param name="other">Instance of Hook to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Hook other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    Id == other.Id ||
                    Id != null &&
                    Id.Equals(other.Id)
                ) && 
                (
                    Url == other.Url ||
                    Url != null &&
                    Url.Equals(other.Url)
                ) && 
                (
                    CreatedAt == other.CreatedAt ||
                    CreatedAt != null &&
                    CreatedAt.Equals(other.CreatedAt)
                ) && 
                (
                    Password == other.Password ||
                    Password != null &&
                    Password.Equals(other.Password)
                ) && 
                (
                    ProjectId == other.ProjectId ||
                    ProjectId != null &&
                    ProjectId.Equals(other.ProjectId)
                ) && 
                (
                    Result == other.Result ||
                    Result != null &&
                    Result.Equals(other.Result)
                ) && 
                (
                    ResultCode == other.ResultCode ||
                    ResultCode != null &&
                    ResultCode.Equals(other.ResultCode)
                ) && 
                (
                    PushEvents == other.PushEvents ||
                    PushEvents != null &&
                    PushEvents.Equals(other.PushEvents)
                ) && 
                (
                    TagPushEvents == other.TagPushEvents ||
                    TagPushEvents != null &&
                    TagPushEvents.Equals(other.TagPushEvents)
                ) && 
                (
                    IssuesEvents == other.IssuesEvents ||
                    IssuesEvents != null &&
                    IssuesEvents.Equals(other.IssuesEvents)
                ) && 
                (
                    NoteEvents == other.NoteEvents ||
                    NoteEvents != null &&
                    NoteEvents.Equals(other.NoteEvents)
                ) && 
                (
                    MergeRequestsEvents == other.MergeRequestsEvents ||
                    MergeRequestsEvents != null &&
                    MergeRequestsEvents.Equals(other.MergeRequestsEvents)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (Id != null)
                    hashCode = hashCode * 59 + Id.GetHashCode();
                    if (Url != null)
                    hashCode = hashCode * 59 + Url.GetHashCode();
                    if (CreatedAt != null)
                    hashCode = hashCode * 59 + CreatedAt.GetHashCode();
                    if (Password != null)
                    hashCode = hashCode * 59 + Password.GetHashCode();
                    if (ProjectId != null)
                    hashCode = hashCode * 59 + ProjectId.GetHashCode();
                    if (Result != null)
                    hashCode = hashCode * 59 + Result.GetHashCode();
                    if (ResultCode != null)
                    hashCode = hashCode * 59 + ResultCode.GetHashCode();
                    if (PushEvents != null)
                    hashCode = hashCode * 59 + PushEvents.GetHashCode();
                    if (TagPushEvents != null)
                    hashCode = hashCode * 59 + TagPushEvents.GetHashCode();
                    if (IssuesEvents != null)
                    hashCode = hashCode * 59 + IssuesEvents.GetHashCode();
                    if (NoteEvents != null)
                    hashCode = hashCode * 59 + NoteEvents.GetHashCode();
                    if (MergeRequestsEvents != null)
                    hashCode = hashCode * 59 + MergeRequestsEvents.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(Hook left, Hook right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Hook left, Hook right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
