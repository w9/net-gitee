/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.1.297
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 更新文件节点的权限
    /// </summary>
    [DataContract]
    public partial class DocNode : IEquatable<DocNode>
    { 
        /// <summary>
        /// id
        /// </summary>
        /// <value>id</value>
        [DataMember(Name="id")]
        public int? Id { get; set; }

        /// <summary>
        /// 父层级的 id
        /// </summary>
        /// <value>父层级的 id</value>
        [DataMember(Name="parent_id")]
        public int? ParentId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        /// <value>名称</value>
        [DataMember(Name="name")]
        public string Name { get; set; }

        /// <summary>
        /// 权限值
        /// </summary>
        /// <value>权限值</value>
        [DataMember(Name="public")]
        public string Public { get; set; }

        /// <summary>
        /// 权限名称
        /// </summary>
        /// <value>权限名称</value>
        [DataMember(Name="public_name")]
        public string PublicName { get; set; }

        /// <summary>
        /// Gets or Sets Program
        /// </summary>
        [DataMember(Name="program")]
        public Program Program { get; set; }

        /// <summary>
        /// 关联类型。(目录：DocDirectory，文档：WikiInfo，附件：AttachFile)
        /// </summary>
        /// <value>关联类型。(目录：DocDirectory，文档：WikiInfo，附件：AttachFile)</value>
        [DataMember(Name="file_type")]
        public string FileType { get; set; }

        /// <summary>
        /// 关联类型的 id
        /// </summary>
        /// <value>关联类型的 id</value>
        [DataMember(Name="file_id")]
        public string FileId { get; set; }

        /// <summary>
        /// 是否查看附件历史版本
        /// </summary>
        /// <value>是否查看附件历史版本</value>
        [DataMember(Name="file_versions")]
        public bool? FileVersions { get; set; }

        /// <summary>
        /// Gets or Sets Creator
        /// </summary>
        [DataMember(Name="creator")]
        public UserWithRemark Creator { get; set; }

        /// <summary>
        /// 是否已收藏
        /// </summary>
        /// <value>是否已收藏</value>
        [DataMember(Name="is_favour")]
        public bool? IsFavour { get; set; }

        /// <summary>
        /// Gets or Sets AttachFile
        /// </summary>
        [DataMember(Name="attach_file")]
        public DocAttachFile AttachFile { get; set; }

        /// <summary>
        /// 是否 wiki
        /// </summary>
        /// <value>是否 wiki</value>
        [DataMember(Name="is_wiki")]
        public bool? IsWiki { get; set; }

        /// <summary>
        /// 需要密码访问
        /// </summary>
        /// <value>需要密码访问</value>
        [DataMember(Name="need_password")]
        public bool? NeedPassword { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class DocNode {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  ParentId: ").Append(ParentId).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  Public: ").Append(Public).Append("\n");
            sb.Append("  PublicName: ").Append(PublicName).Append("\n");
            sb.Append("  Program: ").Append(Program).Append("\n");
            sb.Append("  FileType: ").Append(FileType).Append("\n");
            sb.Append("  FileId: ").Append(FileId).Append("\n");
            sb.Append("  FileVersions: ").Append(FileVersions).Append("\n");
            sb.Append("  Creator: ").Append(Creator).Append("\n");
            sb.Append("  IsFavour: ").Append(IsFavour).Append("\n");
            sb.Append("  AttachFile: ").Append(AttachFile).Append("\n");
            sb.Append("  IsWiki: ").Append(IsWiki).Append("\n");
            sb.Append("  NeedPassword: ").Append(NeedPassword).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((DocNode)obj);
        }

        /// <summary>
        /// Returns true if DocNode instances are equal
        /// </summary>
        /// <param name="other">Instance of DocNode to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(DocNode other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    Id == other.Id ||
                    Id != null &&
                    Id.Equals(other.Id)
                ) && 
                (
                    ParentId == other.ParentId ||
                    ParentId != null &&
                    ParentId.Equals(other.ParentId)
                ) && 
                (
                    Name == other.Name ||
                    Name != null &&
                    Name.Equals(other.Name)
                ) && 
                (
                    Public == other.Public ||
                    Public != null &&
                    Public.Equals(other.Public)
                ) && 
                (
                    PublicName == other.PublicName ||
                    PublicName != null &&
                    PublicName.Equals(other.PublicName)
                ) && 
                (
                    Program == other.Program ||
                    Program != null &&
                    Program.Equals(other.Program)
                ) && 
                (
                    FileType == other.FileType ||
                    FileType != null &&
                    FileType.Equals(other.FileType)
                ) && 
                (
                    FileId == other.FileId ||
                    FileId != null &&
                    FileId.Equals(other.FileId)
                ) && 
                (
                    FileVersions == other.FileVersions ||
                    FileVersions != null &&
                    FileVersions.Equals(other.FileVersions)
                ) && 
                (
                    Creator == other.Creator ||
                    Creator != null &&
                    Creator.Equals(other.Creator)
                ) && 
                (
                    IsFavour == other.IsFavour ||
                    IsFavour != null &&
                    IsFavour.Equals(other.IsFavour)
                ) && 
                (
                    AttachFile == other.AttachFile ||
                    AttachFile != null &&
                    AttachFile.Equals(other.AttachFile)
                ) && 
                (
                    IsWiki == other.IsWiki ||
                    IsWiki != null &&
                    IsWiki.Equals(other.IsWiki)
                ) && 
                (
                    NeedPassword == other.NeedPassword ||
                    NeedPassword != null &&
                    NeedPassword.Equals(other.NeedPassword)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (Id != null)
                    hashCode = hashCode * 59 + Id.GetHashCode();
                    if (ParentId != null)
                    hashCode = hashCode * 59 + ParentId.GetHashCode();
                    if (Name != null)
                    hashCode = hashCode * 59 + Name.GetHashCode();
                    if (Public != null)
                    hashCode = hashCode * 59 + Public.GetHashCode();
                    if (PublicName != null)
                    hashCode = hashCode * 59 + PublicName.GetHashCode();
                    if (Program != null)
                    hashCode = hashCode * 59 + Program.GetHashCode();
                    if (FileType != null)
                    hashCode = hashCode * 59 + FileType.GetHashCode();
                    if (FileId != null)
                    hashCode = hashCode * 59 + FileId.GetHashCode();
                    if (FileVersions != null)
                    hashCode = hashCode * 59 + FileVersions.GetHashCode();
                    if (Creator != null)
                    hashCode = hashCode * 59 + Creator.GetHashCode();
                    if (IsFavour != null)
                    hashCode = hashCode * 59 + IsFavour.GetHashCode();
                    if (AttachFile != null)
                    hashCode = hashCode * 59 + AttachFile.GetHashCode();
                    if (IsWiki != null)
                    hashCode = hashCode * 59 + IsWiki.GetHashCode();
                    if (NeedPassword != null)
                    hashCode = hashCode * 59 + NeedPassword.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(DocNode left, DocNode right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(DocNode left, DocNode right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
