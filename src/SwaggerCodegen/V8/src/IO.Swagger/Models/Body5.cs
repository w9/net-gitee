/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.1.297
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class Body5 : IEquatable<Body5>
    { 
        /// <summary>
        /// 用户授权码
        /// </summary>
        /// <value>用户授权码</value>
        [DataMember(Name="access_token")]
        public string AccessToken { get; set; }

        /// <summary>
        /// 企业角色的 ID
        /// </summary>
        /// <value>企业角色的 ID</value>
        [DataMember(Name="role_id")]
        public int? RoleId { get; set; }

        /// <summary>
        /// 是否需要管理员审核。1：需要，0：不需要
        /// </summary>
        /// <value>是否需要管理员审核。1：需要，0：不需要</value>
        
        public enum NeedCheckEnum
        {
            /// <summary>
            /// Enum _0Enum for 0
            /// </summary>
            
            _0Enum = 0,
            /// <summary>
            /// Enum _1Enum for 1
            /// </summary>
            
            _1Enum = 1        }

        /// <summary>
        /// 是否需要管理员审核。1：需要，0：不需要
        /// </summary>
        /// <value>是否需要管理员审核。1：需要，0：不需要</value>
        [DataMember(Name="need_check")]
        public NeedCheckEnum? NeedCheck { get; set; }

        /// <summary>
        /// 邮件列表，逗号(,)隔开。每次邀请最多只能发送20封邀请邮件
        /// </summary>
        /// <value>邮件列表，逗号(,)隔开。每次邀请最多只能发送20封邀请邮件</value>
        [Required]
        [DataMember(Name="emails")]
        public string Emails { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Body5 {\n");
            sb.Append("  AccessToken: ").Append(AccessToken).Append("\n");
            sb.Append("  RoleId: ").Append(RoleId).Append("\n");
            sb.Append("  NeedCheck: ").Append(NeedCheck).Append("\n");
            sb.Append("  Emails: ").Append(Emails).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Body5)obj);
        }

        /// <summary>
        /// Returns true if Body5 instances are equal
        /// </summary>
        /// <param name="other">Instance of Body5 to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Body5 other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    AccessToken == other.AccessToken ||
                    AccessToken != null &&
                    AccessToken.Equals(other.AccessToken)
                ) && 
                (
                    RoleId == other.RoleId ||
                    RoleId != null &&
                    RoleId.Equals(other.RoleId)
                ) && 
                (
                    NeedCheck == other.NeedCheck ||
                    NeedCheck != null &&
                    NeedCheck.Equals(other.NeedCheck)
                ) && 
                (
                    Emails == other.Emails ||
                    Emails != null &&
                    Emails.Equals(other.Emails)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (AccessToken != null)
                    hashCode = hashCode * 59 + AccessToken.GetHashCode();
                    if (RoleId != null)
                    hashCode = hashCode * 59 + RoleId.GetHashCode();
                    if (NeedCheck != null)
                    hashCode = hashCode * 59 + NeedCheck.GetHashCode();
                    if (Emails != null)
                    hashCode = hashCode * 59 + Emails.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(Body5 left, Body5 right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Body5 left, Body5 right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
