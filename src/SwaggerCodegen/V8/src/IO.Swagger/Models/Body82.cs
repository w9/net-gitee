/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.1.297
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class Body82 : IEquatable<Body82>
    { 
        /// <summary>
        /// 用户授权码
        /// </summary>
        /// <value>用户授权码</value>
        [DataMember(Name="access_token")]
        public string AccessToken { get; set; }

        /// <summary>
        /// 文档名称
        /// </summary>
        /// <value>文档名称</value>
        [Required]
        [DataMember(Name="name")]
        public string Name { get; set; }

        /// <summary>
        /// 所属文件夹的 doc_node 的 id，默认为 0
        /// </summary>
        /// <value>所属文件夹的 doc_node 的 id，默认为 0</value>
        [DataMember(Name="parent_id")]
        public int? ParentId { get; set; }

        /// <summary>
        /// 项目 id
        /// </summary>
        /// <value>项目 id</value>
        [DataMember(Name="program_id")]
        public int? ProgramId { get; set; }

        /// <summary>
        /// 权限; 私有: private; 只读 read_only; 读写: read_write
        /// </summary>
        /// <value>权限; 私有: private; 只读 read_only; 读写: read_write</value>
        [DataMember(Name="auth_type")]
        public string AuthType { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Body82 {\n");
            sb.Append("  AccessToken: ").Append(AccessToken).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  ParentId: ").Append(ParentId).Append("\n");
            sb.Append("  ProgramId: ").Append(ProgramId).Append("\n");
            sb.Append("  AuthType: ").Append(AuthType).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Body82)obj);
        }

        /// <summary>
        /// Returns true if Body82 instances are equal
        /// </summary>
        /// <param name="other">Instance of Body82 to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Body82 other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    AccessToken == other.AccessToken ||
                    AccessToken != null &&
                    AccessToken.Equals(other.AccessToken)
                ) && 
                (
                    Name == other.Name ||
                    Name != null &&
                    Name.Equals(other.Name)
                ) && 
                (
                    ParentId == other.ParentId ||
                    ParentId != null &&
                    ParentId.Equals(other.ParentId)
                ) && 
                (
                    ProgramId == other.ProgramId ||
                    ProgramId != null &&
                    ProgramId.Equals(other.ProgramId)
                ) && 
                (
                    AuthType == other.AuthType ||
                    AuthType != null &&
                    AuthType.Equals(other.AuthType)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (AccessToken != null)
                    hashCode = hashCode * 59 + AccessToken.GetHashCode();
                    if (Name != null)
                    hashCode = hashCode * 59 + Name.GetHashCode();
                    if (ParentId != null)
                    hashCode = hashCode * 59 + ParentId.GetHashCode();
                    if (ProgramId != null)
                    hashCode = hashCode * 59 + ProgramId.GetHashCode();
                    if (AuthType != null)
                    hashCode = hashCode * 59 + AuthType.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(Body82 left, Body82 right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Body82 left, Body82 right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
