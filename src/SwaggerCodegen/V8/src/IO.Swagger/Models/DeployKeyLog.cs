/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.1.297
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 企业部署公钥管理日志
    /// </summary>
    [DataContract]
    public partial class DeployKeyLog : IEquatable<DeployKeyLog>
    { 
        /// <summary>
        /// ID
        /// </summary>
        /// <value>ID</value>
        [DataMember(Name="id")]
        public int? Id { get; set; }

        /// <summary>
        /// 用户ID
        /// </summary>
        /// <value>用户ID</value>
        [DataMember(Name="user_id")]
        public int? UserId { get; set; }

        /// <summary>
        /// Gets or Sets User
        /// </summary>
        [DataMember(Name="user")]
        public UserWithRemark User { get; set; }

        /// <summary>
        /// ip
        /// </summary>
        /// <value>ip</value>
        [DataMember(Name="ip")]
        public string Ip { get; set; }

        /// <summary>
        /// 目标操作对象ID
        /// </summary>
        /// <value>目标操作对象ID</value>
        [DataMember(Name="target_id")]
        public int? TargetId { get; set; }

        /// <summary>
        /// 操作对象名称
        /// </summary>
        /// <value>操作对象名称</value>
        [DataMember(Name="title")]
        public string Title { get; set; }

        /// <summary>
        /// 操作对象类型
        /// </summary>
        /// <value>操作对象类型</value>
        [DataMember(Name="type")]
        public string Type { get; set; }

        /// <summary>
        /// 操作
        /// </summary>
        /// <value>操作</value>
        [DataMember(Name="operating")]
        public string Operating { get; set; }

        /// <summary>
        /// Gets or Sets TargetProject
        /// </summary>
        [DataMember(Name="target_project")]
        public Project TargetProject { get; set; }

        /// <summary>
        /// 仓库名称
        /// </summary>
        /// <value>仓库名称</value>
        [DataMember(Name="target_project_name")]
        public string TargetProjectName { get; set; }

        /// <summary>
        /// 公钥名称
        /// </summary>
        /// <value>公钥名称</value>
        [DataMember(Name="target_deploy_key_title")]
        public string TargetDeployKeyTitle { get; set; }

        /// <summary>
        /// Gets or Sets TargetDeployKey
        /// </summary>
        [DataMember(Name="target_deploy_key")]
        public DeployKey TargetDeployKey { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class DeployKeyLog {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  UserId: ").Append(UserId).Append("\n");
            sb.Append("  User: ").Append(User).Append("\n");
            sb.Append("  Ip: ").Append(Ip).Append("\n");
            sb.Append("  TargetId: ").Append(TargetId).Append("\n");
            sb.Append("  Title: ").Append(Title).Append("\n");
            sb.Append("  Type: ").Append(Type).Append("\n");
            sb.Append("  Operating: ").Append(Operating).Append("\n");
            sb.Append("  TargetProject: ").Append(TargetProject).Append("\n");
            sb.Append("  TargetProjectName: ").Append(TargetProjectName).Append("\n");
            sb.Append("  TargetDeployKeyTitle: ").Append(TargetDeployKeyTitle).Append("\n");
            sb.Append("  TargetDeployKey: ").Append(TargetDeployKey).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((DeployKeyLog)obj);
        }

        /// <summary>
        /// Returns true if DeployKeyLog instances are equal
        /// </summary>
        /// <param name="other">Instance of DeployKeyLog to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(DeployKeyLog other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    Id == other.Id ||
                    Id != null &&
                    Id.Equals(other.Id)
                ) && 
                (
                    UserId == other.UserId ||
                    UserId != null &&
                    UserId.Equals(other.UserId)
                ) && 
                (
                    User == other.User ||
                    User != null &&
                    User.Equals(other.User)
                ) && 
                (
                    Ip == other.Ip ||
                    Ip != null &&
                    Ip.Equals(other.Ip)
                ) && 
                (
                    TargetId == other.TargetId ||
                    TargetId != null &&
                    TargetId.Equals(other.TargetId)
                ) && 
                (
                    Title == other.Title ||
                    Title != null &&
                    Title.Equals(other.Title)
                ) && 
                (
                    Type == other.Type ||
                    Type != null &&
                    Type.Equals(other.Type)
                ) && 
                (
                    Operating == other.Operating ||
                    Operating != null &&
                    Operating.Equals(other.Operating)
                ) && 
                (
                    TargetProject == other.TargetProject ||
                    TargetProject != null &&
                    TargetProject.Equals(other.TargetProject)
                ) && 
                (
                    TargetProjectName == other.TargetProjectName ||
                    TargetProjectName != null &&
                    TargetProjectName.Equals(other.TargetProjectName)
                ) && 
                (
                    TargetDeployKeyTitle == other.TargetDeployKeyTitle ||
                    TargetDeployKeyTitle != null &&
                    TargetDeployKeyTitle.Equals(other.TargetDeployKeyTitle)
                ) && 
                (
                    TargetDeployKey == other.TargetDeployKey ||
                    TargetDeployKey != null &&
                    TargetDeployKey.Equals(other.TargetDeployKey)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (Id != null)
                    hashCode = hashCode * 59 + Id.GetHashCode();
                    if (UserId != null)
                    hashCode = hashCode * 59 + UserId.GetHashCode();
                    if (User != null)
                    hashCode = hashCode * 59 + User.GetHashCode();
                    if (Ip != null)
                    hashCode = hashCode * 59 + Ip.GetHashCode();
                    if (TargetId != null)
                    hashCode = hashCode * 59 + TargetId.GetHashCode();
                    if (Title != null)
                    hashCode = hashCode * 59 + Title.GetHashCode();
                    if (Type != null)
                    hashCode = hashCode * 59 + Type.GetHashCode();
                    if (Operating != null)
                    hashCode = hashCode * 59 + Operating.GetHashCode();
                    if (TargetProject != null)
                    hashCode = hashCode * 59 + TargetProject.GetHashCode();
                    if (TargetProjectName != null)
                    hashCode = hashCode * 59 + TargetProjectName.GetHashCode();
                    if (TargetDeployKeyTitle != null)
                    hashCode = hashCode * 59 + TargetDeployKeyTitle.GetHashCode();
                    if (TargetDeployKey != null)
                    hashCode = hashCode * 59 + TargetDeployKey.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(DeployKeyLog left, DeployKeyLog right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(DeployKeyLog left, DeployKeyLog right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
