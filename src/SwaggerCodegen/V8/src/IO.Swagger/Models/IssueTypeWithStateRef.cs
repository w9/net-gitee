/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.1.297
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 任务类型详情(状态管理)
    /// </summary>
    [DataContract]
    public partial class IssueTypeWithStateRef : IEquatable<IssueTypeWithStateRef>
    { 
        /// <summary>
        /// 任务类型 ID
        /// </summary>
        /// <value>任务类型 ID</value>
        [DataMember(Name="id")]
        public int? Id { get; set; }

        /// <summary>
        /// 任务类型的名称
        /// </summary>
        /// <value>任务类型的名称</value>
        [DataMember(Name="title")]
        public string Title { get; set; }

        /// <summary>
        /// 任务类型模板
        /// </summary>
        /// <value>任务类型模板</value>
        [DataMember(Name="template")]
        public string Template { get; set; }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        /// <value>唯一标识符</value>
        [DataMember(Name="ident")]
        public string Ident { get; set; }

        /// <summary>
        /// 颜色
        /// </summary>
        /// <value>颜色</value>
        [DataMember(Name="color")]
        public string Color { get; set; }

        /// <summary>
        /// 是否系统默认类型
        /// </summary>
        /// <value>是否系统默认类型</value>
        [DataMember(Name="is_system")]
        public bool? IsSystem { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class IssueTypeWithStateRef {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  Title: ").Append(Title).Append("\n");
            sb.Append("  Template: ").Append(Template).Append("\n");
            sb.Append("  Ident: ").Append(Ident).Append("\n");
            sb.Append("  Color: ").Append(Color).Append("\n");
            sb.Append("  IsSystem: ").Append(IsSystem).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((IssueTypeWithStateRef)obj);
        }

        /// <summary>
        /// Returns true if IssueTypeWithStateRef instances are equal
        /// </summary>
        /// <param name="other">Instance of IssueTypeWithStateRef to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(IssueTypeWithStateRef other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    Id == other.Id ||
                    Id != null &&
                    Id.Equals(other.Id)
                ) && 
                (
                    Title == other.Title ||
                    Title != null &&
                    Title.Equals(other.Title)
                ) && 
                (
                    Template == other.Template ||
                    Template != null &&
                    Template.Equals(other.Template)
                ) && 
                (
                    Ident == other.Ident ||
                    Ident != null &&
                    Ident.Equals(other.Ident)
                ) && 
                (
                    Color == other.Color ||
                    Color != null &&
                    Color.Equals(other.Color)
                ) && 
                (
                    IsSystem == other.IsSystem ||
                    IsSystem != null &&
                    IsSystem.Equals(other.IsSystem)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (Id != null)
                    hashCode = hashCode * 59 + Id.GetHashCode();
                    if (Title != null)
                    hashCode = hashCode * 59 + Title.GetHashCode();
                    if (Template != null)
                    hashCode = hashCode * 59 + Template.GetHashCode();
                    if (Ident != null)
                    hashCode = hashCode * 59 + Ident.GetHashCode();
                    if (Color != null)
                    hashCode = hashCode * 59 + Color.GetHashCode();
                    if (IsSystem != null)
                    hashCode = hashCode * 59 + IsSystem.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(IssueTypeWithStateRef left, IssueTypeWithStateRef right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(IssueTypeWithStateRef left, IssueTypeWithStateRef right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
