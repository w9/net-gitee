/*
 * Gitee Open API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 0.1.297
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.SwaggerGen;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using IO.Swagger.Attributes;

using Microsoft.AspNetCore.Authorization;
using IO.Swagger.Models;

namespace IO.Swagger.Controllers
{ 
    /// <summary>
    /// 
    /// </summary>
    [ApiController]
    public class ProjectMembersApiController : ControllerBase
    { 
        /// <summary>
        /// 删除仓库成员申请
        /// </summary>
        /// <remarks>删除仓库成员申请</remarks>
        /// <param name="enterpriseId">企业id</param>
        /// <param name="projectId">仓库 id 或 path</param>
        /// <param name="applyId">成员申请id</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="qt">path类型（查询参数为path）, 空则表示查询参数为id</param>
        /// <response code="204">删除仓库成员申请</response>
        [HttpDelete]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/projects/{project_id}/members/apply/{apply_id}")]
        [ValidateModelState]
        [SwaggerOperation("DeleteEnterpriseIdProjectsProjectIdMembersApplyApplyId")]
        public virtual IActionResult DeleteEnterpriseIdProjectsProjectIdMembersApplyApplyId([FromRoute][Required]int? enterpriseId, [FromRoute][Required]string projectId, [FromRoute][Required]int? applyId, [FromQuery]string accessToken, [FromQuery]string qt)
        { 
            //TODO: Uncomment the next line to return response 204 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(204);

            throw new NotImplementedException();
        }

        /// <summary>
        /// 移除仓库成员
        /// </summary>
        /// <remarks>移除仓库成员</remarks>
        /// <param name="enterpriseId">企业id</param>
        /// <param name="projectId">仓库 id 或 path</param>
        /// <param name="memberId">成员id</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="qt">path类型（查询参数为path）, 空则表示查询参数为id</param>
        /// <response code="204">移除仓库成员</response>
        [HttpDelete]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/projects/{project_id}/members/{member_id}")]
        [ValidateModelState]
        [SwaggerOperation("DeleteEnterpriseIdProjectsProjectIdMembersMemberId")]
        public virtual IActionResult DeleteEnterpriseIdProjectsProjectIdMembersMemberId([FromRoute][Required]int? enterpriseId, [FromRoute][Required]string projectId, [FromRoute][Required]int? memberId, [FromQuery]string accessToken, [FromQuery]string qt)
        { 
            //TODO: Uncomment the next line to return response 204 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(204);

            throw new NotImplementedException();
        }

        /// <summary>
        /// 仓库成员
        /// </summary>
        /// <remarks>仓库成员</remarks>
        /// <param name="enterpriseId">企业id</param>
        /// <param name="projectId">仓库 id 或 path</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="qt">path类型（查询参数为path）, 空则表示查询参数为id</param>
        /// <param name="accessLevel">reporter:报告者, viewer:观察者, developer:开发者, master:管理员</param>
        /// <param name="scope">not_in:获取不在本仓库的企业成员</param>
        /// <param name="search">成员搜索，邮箱、username、name、remark</param>
        /// <param name="page">当前的页码</param>
        /// <param name="perPage">每页的数量，最大为 100</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/projects/{project_id}/members")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdProjectsProjectIdMembers")]
        [SwaggerResponse(statusCode: 200, type: typeof(ProjectMember), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdProjectsProjectIdMembers([FromRoute][Required]int? enterpriseId, [FromRoute][Required]string projectId, [FromQuery]string accessToken, [FromQuery]string qt, [FromQuery]string accessLevel, [FromQuery]string scope, [FromQuery]string search, [FromQuery]int? page, [FromQuery]int? perPage)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(ProjectMember));
            string exampleJson = null;
            exampleJson = "{\n  \"project_access\" : \"project_access\",\n  \"project_access_name\" : \"project_access_name\",\n  \"project_creator\" : true,\n  \"is_myself\" : true,\n  \"target\" : \"target\",\n  \"access_level\" : \"access_level\",\n  \"is_blocked\" : true,\n  \"avatar_url\" : \"avatar_url\",\n  \"allow_admin\" : true,\n  \"project_owner\" : true,\n  \"name\" : \"name\",\n  \"nickname\" : \"nickname\",\n  \"id\" : 0,\n  \"access_level_ident\" : \"access_level_ident\",\n  \"email\" : \"email\",\n  \"username\" : \"username\"\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<ProjectMember>(exampleJson)
                        : default(ProjectMember);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 仓库成员申请列表
        /// </summary>
        /// <remarks>仓库成员申请列表</remarks>
        /// <param name="enterpriseId">企业id</param>
        /// <param name="projectId">仓库 id 或 path</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="qt">path类型（查询参数为path）, 空则表示查询参数为id</param>
        /// <param name="query">搜索字符串：申请人姓名或个人空间地址</param>
        /// <param name="status">状态[approved:已同意,pending:待审核,refused:已拒绝,overdue:已过期,invite_pass:已接受邀请]</param>
        /// <param name="page">当前的页码</param>
        /// <param name="perPage">每页的数量，最大为 100</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/projects/{project_id}/members/apply_list")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdProjectsProjectIdMembersApplyList")]
        [SwaggerResponse(statusCode: 200, type: typeof(MemberApplication), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdProjectsProjectIdMembersApplyList([FromRoute][Required]int? enterpriseId, [FromRoute][Required]string projectId, [FromQuery]string accessToken, [FromQuery]string qt, [FromQuery]string query, [FromQuery]string status, [FromQuery]int? page, [FromQuery]int? perPage)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(MemberApplication));
            string exampleJson = null;
            exampleJson = "{\n  \"access\" : 1,\n  \"role_id\" : 6,\n  \"remark\" : \"remark\",\n  \"id\" : 0,\n  \"need_check\" : \"need_check\",\n  \"state\" : \"state\",\n  \"applicant_name\" : \"applicant_name\",\n  \"operator\" : \"operator\",\n  \"applicant\" : {\n    \"pinyin\" : \"pinyin\",\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"is_enterprise_member\" : true,\n    \"outsourced\" : true,\n    \"remark\" : \"remark\",\n    \"id\" : 6,\n    \"username\" : \"username\"\n  }\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<MemberApplication>(exampleJson)
                        : default(MemberApplication);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 成员概览
        /// </summary>
        /// <remarks>成员概览</remarks>
        /// <param name="enterpriseId">企业id</param>
        /// <param name="projectId">仓库 id 或 path</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="qt">path类型（查询参数为path）, 空则表示查询参数为id</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/projects/{project_id}/members/overview")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdProjectsProjectIdMembersOverview")]
        [SwaggerResponse(statusCode: 200, type: typeof(ProjectMemberOverview), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdProjectsProjectIdMembersOverview([FromRoute][Required]int? enterpriseId, [FromRoute][Required]string projectId, [FromQuery]string accessToken, [FromQuery]string qt)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(ProjectMemberOverview));
            string exampleJson = null;
            exampleJson = "{\n  \"all\" : 5,\n  \"viewer\" : 0,\n  \"padding_apply_count\" : 2,\n  \"reporter\" : 6,\n  \"developer\" : 1,\n  \"allow_external_apply\" : true,\n  \"master\" : 5\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<ProjectMemberOverview>(exampleJson)
                        : default(ProjectMemberOverview);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 获取仓库角色
        /// </summary>
        /// <remarks>获取仓库角色</remarks>
        /// <param name="enterpriseId">企业id</param>
        /// <param name="projectId">仓库 id 或 path</param>
        /// <param name="accessToken">用户授权码</param>
        /// <param name="qt">path类型（查询参数为path）, 空则表示查询参数为id</param>
        /// <response code="200">返回格式</response>
        [HttpGet]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/projects/{project_id}/members/roles")]
        [ValidateModelState]
        [SwaggerOperation("GetEnterpriseIdProjectsProjectIdMembersRoles")]
        [SwaggerResponse(statusCode: 200, type: typeof(ProjectRole), description: "返回格式")]
        public virtual IActionResult GetEnterpriseIdProjectsProjectIdMembersRoles([FromRoute][Required]int? enterpriseId, [FromRoute][Required]string projectId, [FromQuery]string accessToken, [FromQuery]string qt)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(ProjectRole));
            string exampleJson = null;
            exampleJson = "{\n  \"role\" : \"role\",\n  \"access\" : \"access\",\n  \"role_level\" : \"role_level\"\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<ProjectRole>(exampleJson)
                        : default(ProjectRole);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 添加仓库仓库成员
        /// </summary>
        /// <remarks>添加仓库仓库成员</remarks>
        /// <param name="body"></param>
        /// <param name="enterpriseId">企业id</param>
        /// <param name="projectId">仓库 id 或 path</param>
        /// <response code="201">返回格式</response>
        [HttpPost]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/projects/{project_id}/members")]
        [ValidateModelState]
        [SwaggerOperation("PostEnterpriseIdProjectsProjectIdMembers")]
        [SwaggerResponse(statusCode: 201, type: typeof(ProjectMemberAdd), description: "返回格式")]
        public virtual IActionResult PostEnterpriseIdProjectsProjectIdMembers([FromBody]Body52 body, [FromRoute][Required]int? enterpriseId, [FromRoute][Required]string projectId)
        { 
            //TODO: Uncomment the next line to return response 201 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(201, default(ProjectMemberAdd));
            string exampleJson = null;
            exampleJson = "{\n  \"msg\" : \"msg\",\n  \"role_name\" : \"role_name\",\n  \"access\" : \"access\",\n  \"success\" : true,\n  \"name\" : \"name\",\n  \"id\" : 0,\n  \"state\" : \"state\",\n  \"email\" : \"email\",\n  \"username\" : \"username\",\n  \"no_register\" : true\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<ProjectMemberAdd>(exampleJson)
                        : default(ProjectMemberAdd);            //TODO: Change the data returned
            return new ObjectResult(example);
        }

        /// <summary>
        /// 修改仓库成员权限
        /// </summary>
        /// <remarks>修改仓库成员权限</remarks>
        /// <param name="body"></param>
        /// <param name="enterpriseId">企业id</param>
        /// <param name="projectId">仓库 id 或 path</param>
        /// <param name="memberId">成员id</param>
        /// <response code="200">修改仓库成员权限</response>
        [HttpPut]
        [Route("//https://api.gitee.comenterprises/{enterprise_id}/projects/{project_id}/members/{member_id}")]
        [ValidateModelState]
        [SwaggerOperation("PutEnterpriseIdProjectsProjectIdMembersMemberId")]
        [SwaggerResponse(statusCode: 200, type: typeof(Member), description: "修改仓库成员权限")]
        public virtual IActionResult PutEnterpriseIdProjectsProjectIdMembersMemberId([FromBody]Body53 body, [FromRoute][Required]int? enterpriseId, [FromRoute][Required]string projectId, [FromRoute][Required]int? memberId)
        { 
            //TODO: Uncomment the next line to return response 200 or use other options such as return this.NotFound(), return this.BadRequest(..), ...
            // return StatusCode(200, default(Member));
            string exampleJson = null;
            exampleJson = "{\n  \"enterprise_role\" : {\n    \"ident\" : \"ident\",\n    \"name\" : \"name\",\n    \"description\" : \"description\",\n    \"id\" : 5,\n    \"is_system_default\" : true,\n    \"is_default\" : true\n  },\n  \"occupation\" : \"occupation\",\n  \"is_block\" : true,\n  \"remark\" : \"remark\",\n  \"block_message\" : \"block_message\",\n  \"enterprise_version\" : 5,\n  \"is_guided\" : true,\n  \"pinyin\" : \"pinyin\",\n  \"phone\" : \"phone\",\n  \"name\" : \"name\",\n  \"id\" : 0,\n  \"is_feedback\" : true,\n  \"user\" : {\n    \"user_color\" : 1,\n    \"avatar_url\" : \"avatar_url\",\n    \"name\" : \"name\",\n    \"id\" : 6,\n    \"state\" : \"state\",\n    \"username\" : \"username\"\n  },\n  \"email\" : \"email\",\n  \"username\" : \"username\"\n}";
            
                        var example = exampleJson != null
                        ? JsonConvert.DeserializeObject<Member>(exampleJson)
                        : default(Member);            //TODO: Change the data returned
            return new ObjectResult(example);
        }
    }
}
