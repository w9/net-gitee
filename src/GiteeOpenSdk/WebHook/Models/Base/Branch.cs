﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace GiteeOpenSdk.WebHook.Models
{
    public partial class Branch
    {
        /// <summary>
        /// 分支标记。eg：oschina:master
        /// </summary>
        [JsonProperty("label")]
        public string Label { get; set; }
        /// <summary>
        /// 分支名。eg：master
        /// </summary>
        [JsonProperty("ref")]
        public string Ref { get; set; }
        /// <summary>
        /// git 提交记录中 sha 值。eg：51b1acb1b4044fcdb2ff8a75ad15a4b655101754
        /// </summary>
        [JsonProperty("sha")]
        public string Sha { get; set; }
        /// <summary>
        /// 分支所在仓库的所有者信息
        /// </summary>
        [JsonProperty("user")]
        public User User { get; set; }
        /// <summary>
        /// 分支所在仓库的信息
        /// </summary>
        [JsonProperty("repo")]
        public Project Repo { get; set; }

    }
}
