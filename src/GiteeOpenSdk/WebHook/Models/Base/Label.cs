﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiteeOpenSdk.WebHook.Models
{
    public partial class Label
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }
        /// <summary>
        /// 标签名。eg：fixbug
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 标签的颜色。eg：000000
        /// </summary>
        [JsonProperty("color")]
        public string Color { get; set; }
    }
}
