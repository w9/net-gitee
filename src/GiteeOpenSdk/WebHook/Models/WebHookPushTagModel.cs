﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace GiteeOpenSdk.WebHook.Models
{
    /// <summary>
    /// Gitee WebHook Push/Tag 返回模型
    /// </summary>
    public class WebHookPushTagModel : WebHookBaseModel
    {
        /// <summary>
        /// 推送的分支。eg：refs/heads/master
        /// </summary>
        [JsonProperty("ref")]
        public string Ref { get; set; }
        /// <summary>
        /// 推送前分支的 commit id。eg：5221c062df39e9e477ab015df22890b7bf13fbbd
        /// </summary>
        [JsonProperty("before")]
        public string Before { get; set; }
        /// <summary>
        /// 推送后分支的 commit id。eg：1cdcd819599cbb4099289dbbec762452f006cb40
        /// </summary>
        [JsonProperty("after")]
        public string After { get; set; }

        /// <summary>
        /// 推送包含的 commit 总数。
        /// </summary>
        [JsonProperty("total_commits_count")]
        public int? totalCommitsCount { get; set; }
        /// <summary>
        /// 推送包含的 commit 总数是否大于十二。
        /// </summary>
        [JsonProperty("commits_more_than_ten")]
        public bool? CommitsMoreThanTen { get; set; }
        /// <summary>
        /// 推送的是否是新分支。
        /// </summary>
        [JsonProperty("created")]
        public bool Created { get; set; }
        /// <summary>
        /// 推送的是否是删除分支。
        /// </summary>
        [JsonProperty("deleted")]
        public bool Deleted { get; set; }
        /// <summary>
        /// 推送的 commit 差异 url。eg：https://gitee.com/oschina/git-osc/compare/5221c062df39e9e477ab015df22890b7bf13fbbd...1cdcd819599cbb4099289dbbec762452f006cb40
        /// </summary>
        [JsonProperty("compare")]
        public string Compare { get; set; }
        /// <summary>
        /// 推送的全部 commit 信息。
        /// </summary>
        [JsonProperty("commits")]
        public List<Commit> Commits { get; set; }
        /// <summary>
        /// 推送最前面的 commit 信息。
        /// </summary>
        [JsonProperty("head_commit")]
        public Commit HeadCommit { get; set; }
        /// <summary>
        /// 推送的目标仓库信息。
        /// </summary>
        [JsonProperty("repository")]
        public Repository Repository { get; set; }
        /// <summary>
        /// 推送的目标仓库信息。
        /// </summary>
        [JsonProperty("project")]
        public Project Project { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("user_id")]
        public int UserId { get; set; }
        /// <summary>
        /// 推送者的昵称。
        /// </summary>
        [JsonProperty("user_name")]
        public string UserName { get; set; }
        /// <summary>
        /// 推送者的用户信息。
        /// </summary>
        [JsonProperty("user")]
        public User User { get; set; }
        /// <summary>
        /// 推送者的用户信息。
        /// </summary>
        [JsonProperty("pusher")]
        public User Pusher { get; set; }
        /// <summary>
        /// 推送者的用户信息。
        /// </summary>
        [JsonProperty("sender")]
        public User Sender { get; set; }
        /// <summary>
        /// issue 所属的企业信息。
        /// </summary>
        [JsonProperty("enterprise")]
        public Enterprise Enterprise { get; set; }
    }
}