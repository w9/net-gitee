﻿using System;
namespace GiteeOpenSdk.Models.Response.Issues
{
    /// <summary>
    /// 更新企业的某个Issue
    /// </summary>
    public class PatchV5EnterprisesEnterpriseIssuesNumberResponseModel : IssueBaseResponseModel
    {
    }
}
