﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GiteeOpenSdk.Models.BaseModels;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Response.Users
{
    /// <summary>
    /// 列出授权用户的所有公钥的返回模型
    /// </summary>
    public class GetV5UserKeys : KeyModel
    {

    }

}
