﻿using System;
using GiteeOpenSdk.Models.BaseModels;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Response.Users
{
    /// <summary>
    /// 获取授权用户的一个 Namespace
    /// </summary>
    public class GetV5UserNamespaceResponseModel : NameSpaceModel
    {
        [JsonProperty("parent")]
        public NameSpaceModel Parent { get; set; }
    }
}
