﻿using System;
using GiteeOpenSdk.Models.BaseModels;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Response.Users
{
    /// <summary>
    /// 列出指定用户的关注者
    /// </summary>
    public class GetV5UsersUsernameFollowersResponseModel : UserModel
    {

    }
}
