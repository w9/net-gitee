using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Users
{
    /// <summary>
    /// 更新授权用户的资料
    /// </summary>
    [RequestMethod("PATCH")]
    public partial class PatchV5UserRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 微博链接
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("blog")]
        public string Blog { get; set; }
        /// <summary>
        /// 博客站点
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("weibo")]
        public string Weibo { get; set; }
        /// <summary>
        /// 自我介绍
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("bio")]
        public string Bio { get; set; }

    }
}