using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Users
{
    /// <summary>
    /// 获取授权用户的一个 Namespace
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5UserNamespaceRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// Namespace path
        /// </summary>
        [RequestType("query")]
        [JsonProperty("path")]
        public string Path { get; set; }

    }
}