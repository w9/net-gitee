using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Users
{
    /// <summary>
    /// 列出指定用户的关注者
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5UsersUsernameFollowersRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 用户名(username/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }

    }
}