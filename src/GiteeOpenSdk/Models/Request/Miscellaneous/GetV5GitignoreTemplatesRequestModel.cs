using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Miscellaneous
{
    /// <summary>
    /// 列出可使用的 .gitignore 模板
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5GitignoreTemplatesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

    }
}