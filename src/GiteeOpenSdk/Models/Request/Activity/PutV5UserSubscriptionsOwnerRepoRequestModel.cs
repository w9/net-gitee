using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Activity
{
    /// <summary>
    /// watch 一个仓库
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutV5UserSubscriptionsOwnerRepoRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// watch策略, watching: 关注所有动态, ignoring: 关注但不提醒动态
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("watch_type")]
        public string WatchType { get; set; }

    }
}