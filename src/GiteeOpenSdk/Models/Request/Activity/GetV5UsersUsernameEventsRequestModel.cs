using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Activity
{
    /// <summary>
    /// 列出用户的动态
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5UsersUsernameEventsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 用户名(username/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// 滚动列表的最后一条记录的id
        /// </summary>
        [RequestType("query")]
        [JsonProperty("prev_id")]
        public int? PrevId { get; set; }
        /// <summary>
        /// 滚动列表每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("limit")]
        public int? Limit { get; set; }

    }
}