using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Search
{
    /// <summary>
    /// 搜索用户
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5SearchUsersRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 搜索关键字
        /// </summary>
        [RequestType("query")]
        [JsonProperty("q")]
        public string Q { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }
        /// <summary>
        /// 排序字段，joined_at(注册时间)，默认为最佳匹配
        /// </summary>
        [RequestType("query")]
        [JsonProperty("sort")]
        public string Sort { get; set; }
        /// <summary>
        /// 排序顺序: desc(default)、asc
        /// </summary>
        [RequestType("query")]
        [JsonProperty("order")]
        public string Order { get; set; }

    }
}