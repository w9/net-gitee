using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Search
{
    /// <summary>
    /// 搜索仓库
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5SearchRepositoriesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 搜索关键字
        /// </summary>
        [RequestType("query")]
        [JsonProperty("q")]
        public string Q { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }
        /// <summary>
        /// 筛选指定空间地址(企业、组织或个人的地址 path) 的仓库
        /// </summary>
        [RequestType("query")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 是否搜索含 fork 的仓库，默认：否
        /// </summary>
        [RequestType("query")]
        [JsonProperty("fork")]
        public bool? Fork { get; set; }
        /// <summary>
        /// 筛选指定语言的仓库
        /// </summary>
        [RequestType("query")]
        [JsonProperty("language")]
        public string Language { get; set; }
        /// <summary>
        /// 排序字段，last_push_at(更新时间)、stars_count(收藏数)、forks_count(Fork 数)、watches_count(关注数)，默认为最佳匹配
        /// </summary>
        [RequestType("query")]
        [JsonProperty("sort")]
        public string Sort { get; set; }
        /// <summary>
        /// 排序顺序: desc(default)、asc
        /// </summary>
        [RequestType("query")]
        [JsonProperty("order")]
        public string Order { get; set; }

    }
}