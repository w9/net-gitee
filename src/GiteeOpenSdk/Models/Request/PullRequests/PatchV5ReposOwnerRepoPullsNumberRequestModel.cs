using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.PullRequests
{
    /// <summary>
    /// 更新Pull Request信息
    /// </summary>
    [RequestMethod("PATCH")]
    public partial class PatchV5ReposOwnerRepoPullsNumberRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 第几个PR，即本仓库PR的序数
        /// </summary>
        [RequestType("path")]
        [JsonProperty("number")]
        public int Number { get; set; }
        /// <summary>
        /// 可选。Pull Request 标题
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// 可选。Pull Request 内容
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("body")]
        public string Body { get; set; }
        /// <summary>
        /// 可选。Pull Request 状态
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 可选。里程碑序号(id)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("milestone_number")]
        public int? MilestoneNumber { get; set; }
        /// <summary>
        /// 用逗号分开的标签，名称要求长度在 2-20 之间且非特殊字符。如: bug,performance
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("labels")]
        public string Labels { get; set; }
        /// <summary>
        /// 可选。最少审查人数
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("assignees_number")]
        public int? AssigneesNumber { get; set; }
        /// <summary>
        /// 可选。最少测试人数
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("testers_number")]
        public int? TestersNumber { get; set; }

    }
}