using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.PullRequests
{
    /// <summary>
    /// 获取Pull Request列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5ReposOwnerRepoPullsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 可选。Pull Request 状态
        /// </summary>
        [RequestType("query")]
        [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 可选。Pull Request 提交的源分支。格式：branch 或者：username:branch
        /// </summary>
        [RequestType("query")]
        [JsonProperty("head")]
        public string Head { get; set; }
        /// <summary>
        /// 可选。Pull Request 提交目标分支的名称。
        /// </summary>
        [RequestType("query")]
        [JsonProperty("base")]
        public string Base { get; set; }
        /// <summary>
        /// 可选。排序字段，默认按创建时间
        /// </summary>
        [RequestType("query")]
        [JsonProperty("sort")]
        public string Sort { get; set; }
        /// <summary>
        /// 可选。起始的更新时间，要求时间格式为 ISO 8601
        /// </summary>
        [RequestType("query")]
        [JsonProperty("since")]
        public string Since { get; set; }
        /// <summary>
        /// 可选。升序/降序
        /// </summary>
        [RequestType("query")]
        [JsonProperty("direction")]
        public string Direction { get; set; }
        /// <summary>
        /// 可选。里程碑序号(id)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("milestone_number")]
        public int? MilestoneNumber { get; set; }
        /// <summary>
        /// 用逗号分开的标签。如: bug,performance
        /// </summary>
        [RequestType("query")]
        [JsonProperty("labels")]
        public string Labels { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }

    }
}