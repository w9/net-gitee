using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.GitData
{
    /// <summary>
    /// 获取文件Blob
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5ReposOwnerRepoGitBlobsShaRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 文件的 Blob SHA，可通过 [获取仓库具体路径下的内容] API 获取
        /// </summary>
        [RequestType("path")]
        [JsonProperty("sha")]
        public string Sha { get; set; }

    }
}