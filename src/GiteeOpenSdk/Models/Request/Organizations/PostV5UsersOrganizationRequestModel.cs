using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Organizations
{
    /// <summary>
    /// 创建组织
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostV5UsersOrganizationRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 组织名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 组织的路径(path/login)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("org")]
        public string Org { get; set; }
        /// <summary>
        /// 组织描述
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("description")]
        public string Description { get; set; }

    }
}