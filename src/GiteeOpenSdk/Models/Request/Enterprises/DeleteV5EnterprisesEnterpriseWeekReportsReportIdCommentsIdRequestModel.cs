using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Enterprises
{
    /// <summary>
    /// 删除周报某个评论
    /// </summary>
    [RequestMethod("DELETE")]
    public partial class DeleteV5EnterprisesEnterpriseWeekReportsReportIdCommentsIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业的路径(path/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise")]
        public string Enterprise { get; set; }
        /// <summary>
        /// 周报ID
        /// </summary>
        [RequestType("path")]
        [JsonProperty("report_id")]
        public int ReportId { get; set; }
        /// <summary>
        /// 评论ID
        /// </summary>
        [RequestType("path")]
        [JsonProperty("id")]
        public int Id { get; set; }

    }
}