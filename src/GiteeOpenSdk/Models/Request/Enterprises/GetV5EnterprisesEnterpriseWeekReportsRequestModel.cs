using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Enterprises
{
    /// <summary>
    /// 企业成员周报列表
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5EnterprisesEnterpriseWeekReportsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业的路径(path/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise")]
        public string Enterprise { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }
        /// <summary>
        /// 用户名(username/login)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// 周报所属年
        /// </summary>
        [RequestType("query")]
        [JsonProperty("year")]
        public int? Year { get; set; }
        /// <summary>
        /// 周报所属周
        /// </summary>
        [RequestType("query")]
        [JsonProperty("week_index")]
        public int? WeekIndex { get; set; }
        /// <summary>
        /// 周报日期(格式：2019-03-25)
        /// </summary>
        [RequestType("query")]
        [JsonProperty("date")]
        public string Date { get; set; }

    }
}