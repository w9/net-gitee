using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Enterprises
{
    /// <summary>
    /// 添加或邀请企业成员
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostV5EnterprisesEnterpriseMembersRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 企业的路径(path/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("enterprise")]
        public string Enterprise { get; set; }
        /// <summary>
        /// 需要邀请的用户名(username/login)，username,email至少填写一个
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("username")]
        public string Username { get; set; }
        /// <summary>
        /// 要添加邮箱地址，若该邮箱未注册则自动创建帐号。username,email至少填写一个
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("email")]
        public string Email { get; set; }
        /// <summary>
        /// 企业角色：member =&gt; 普通成员, outsourced =&gt; 外包成员, admin =&gt; 管理员
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("role")]
        public string Role { get; set; }
        /// <summary>
        /// 企业成员真实姓名（备注）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }

    }
}