using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Repositories
{
    /// <summary>
    /// Fork一个仓库
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostV5ReposOwnerRepoForksRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 组织空间地址，不填写默认Fork到用户个人空间地址
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("organization")]
        public string Organization { get; set; }
        /// <summary>
        /// fork 后仓库名称。默认: 源仓库名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// fork 后仓库地址。默认: 源仓库地址
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("path")]
        public string Path { get; set; }

    }
}