using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Repositories
{
    /// <summary>
    /// 修改代码审查设置
    /// </summary>
    [RequestMethod("PUT")]
    public partial class PutV5ReposOwnerRepoReviewerRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 审查人员username，可多个，半角逗号分隔，如：(username1,username2)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("assignees")]
        public string Assignees { get; set; }
        /// <summary>
        /// 测试人员username，可多个，半角逗号分隔，如：(username1,username2)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("testers")]
        public string Testers { get; set; }
        /// <summary>
        /// 最少审查人数
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("assignees_number")]
        public int AssigneesNumber { get; set; }
        /// <summary>
        /// 最少测试人数
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("testers_number")]
        public int TestersNumber { get; set; }

    }
}