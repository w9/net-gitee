using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Repositories
{
    /// <summary>
    /// 设置/更新仓库的百度统计 key
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostV5ReposOwnerRepoBaiduStatisticKeyRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 通过百度统计页面获取的 hm.js? 后面的 key
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("key")]
        public string Key { get; set; }

    }
}