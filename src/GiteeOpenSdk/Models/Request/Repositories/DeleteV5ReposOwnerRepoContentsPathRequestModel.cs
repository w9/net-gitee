using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Repositories
{
    /// <summary>
    /// 删除文件
    /// </summary>
    [RequestMethod("DELETE")]
    public partial class DeleteV5ReposOwnerRepoContentsPathRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 文件的路径
        /// </summary>
        [RequestType("path")]
        [JsonProperty("path")]
        public string Path { get; set; }
        /// <summary>
        /// 文件的 Blob SHA，可通过 [获取仓库具体路径下的内容] API 获取
        /// </summary>
        [RequestType("query")]
        [JsonProperty("sha")]
        public string Sha { get; set; }
        /// <summary>
        /// 提交信息
        /// </summary>
        [RequestType("query")]
        [JsonProperty("message")]
        public string Message { get; set; }
        /// <summary>
        /// 分支名称。默认为仓库对默认分支
        /// </summary>
        [RequestType("query")]
        [JsonProperty("branch")]
        public string Branch { get; set; }
        /// <summary>
        /// Committer的名字，默认为当前用户的名字
        /// </summary>
        [RequestType("query")]
        [JsonProperty("committer[name]")]
        public string CommitterName { get; set; }
        /// <summary>
        /// Committer的邮箱，默认为当前用户的邮箱
        /// </summary>
        [RequestType("query")]
        [JsonProperty("committer[email]")]
        public string CommitterEmail { get; set; }
        /// <summary>
        /// Author的名字，默认为当前用户的名字
        /// </summary>
        [RequestType("query")]
        [JsonProperty("author[name]")]
        public string AuthorName { get; set; }
        /// <summary>
        /// Author的邮箱，默认为当前用户的邮箱
        /// </summary>
        [RequestType("query")]
        [JsonProperty("author[email]")]
        public string AuthorEmail { get; set; }

    }
}