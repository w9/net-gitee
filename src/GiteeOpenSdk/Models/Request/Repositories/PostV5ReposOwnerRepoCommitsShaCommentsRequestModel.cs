using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Repositories
{
    /// <summary>
    /// 创建Commit评论
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostV5ReposOwnerRepoCommitsShaCommentsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 评论的sha值
        /// </summary>
        [RequestType("path")]
        [JsonProperty("sha")]
        public string Sha { get; set; }
        /// <summary>
        /// 评论的内容
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("body")]
        public string Body { get; set; }
        /// <summary>
        /// 文件的相对路径
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("path")]
        public string Path { get; set; }
        /// <summary>
        /// Diff的相对行数
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("position")]
        public int? Position { get; set; }

    }
}