using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Repositories
{
    /// <summary>
    /// 创建组织仓库
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostV5OrgsOrgReposRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 仓库描述
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("description")]
        public string Description { get; set; }
        /// <summary>
        /// 主页(eg: https://gitee.com)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("homepage")]
        public string Homepage { get; set; }
        /// <summary>
        /// 允许提Issue与否。默认: 允许(true)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("has_issues")]
        public bool? HasIssues { get; set; }
        /// <summary>
        /// 提供Wiki与否。默认: 提供(true)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("has_wiki")]
        public bool? HasWiki { get; set; }
        /// <summary>
        /// 允许用户对仓库进行评论。默认： 允许(true)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("can_comment")]
        public bool? CanComment { get; set; }
        /// <summary>
        /// 组织的路径(path/login)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("org")]
        public string Org { get; set; }
        /// <summary>
        /// 仓库开源类型。0(私有), 1(外部开源), 2(内部开源)，注：与private互斥，以public为主。
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("public")]
        public int? Public { get; set; }
        /// <summary>
        /// 仓库公开或私有。默认: 公开(false)，注：与public互斥，以public为主。
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("private")]
        public bool? Private { get; set; }
        /// <summary>
        /// 值为true时则会用README初始化仓库。默认: 不初始化(false)
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("auto_init")]
        public bool? AutoInit { get; set; }
        /// <summary>
        /// Git Ignore模版
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("gitignore_template")]
        public string GitignoreTemplate { get; set; }
        /// <summary>
        /// License模版
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("license_template")]
        public string LicenseTemplate { get; set; }
        /// <summary>
        /// 仓库路径
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("path")]
        public string Path { get; set; }

    }
}