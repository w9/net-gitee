using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Repositories
{
    /// <summary>
    /// 更新仓库Release
    /// </summary>
    [RequestMethod("PATCH")]
    public partial class PatchV5ReposOwnerRepoReleasesIdRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("tag_name")]
        public string TagName { get; set; }
        /// <summary>
        /// Release 名称
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// Release 描述
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("body")]
        public string Body { get; set; }
        /// <summary>
        /// 是否为预览版本。默认: false（非预览版本）
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("prerelease")]
        public bool? Prerelease { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [RequestType("path")]
        [JsonProperty("id")]
        public int Id { get; set; }

    }
}