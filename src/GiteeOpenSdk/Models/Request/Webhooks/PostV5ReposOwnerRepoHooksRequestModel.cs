using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Webhooks
{
    /// <summary>
    /// 创建一个仓库WebHook
    /// </summary>
    [RequestMethod("POST")]
    public partial class PostV5ReposOwnerRepoHooksRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// 远程HTTP URL
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("url")]
        public string Url { get; set; }
        /// <summary>
        /// 加密类型: 0: 密码, 1: 签名密钥
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("encryption_type")]
        public int? EncryptionType { get; set; }
        /// <summary>
        /// 请求URL时会带上该密码，防止URL被恶意请求
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("password")]
        public string Password { get; set; }
        /// <summary>
        /// Push代码到仓库
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("push_events")]
        public bool? PushEvents { get; set; }
        /// <summary>
        /// 提交Tag到仓库
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("tag_push_events")]
        public bool? TagPushEvents { get; set; }
        /// <summary>
        /// 创建/关闭Issue
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("issues_events")]
        public bool? IssuesEvents { get; set; }
        /// <summary>
        /// 评论了Issue/代码等等
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("note_events")]
        public bool? NoteEvents { get; set; }
        /// <summary>
        /// 合并请求和合并后
        /// </summary>
        [RequestType("formData")]
        [JsonProperty("merge_requests_events")]
        public bool? MergeRequestsEvents { get; set; }

    }
}