using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Issues
{
    /// <summary>
    /// 获取仓库某个Issue所有的评论
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5ReposOwnerRepoIssuesNumberCommentsRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 仓库所属空间地址(企业、组织或个人的地址path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("owner")]
        public string Owner { get; set; }
        /// <summary>
        /// 仓库路径(path)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("repo")]
        public string Repo { get; set; }
        /// <summary>
        /// Issue 编号(区分大小写，无需添加 # 号)
        /// </summary>
        [RequestType("path")]
        [JsonProperty("number")]
        public string Number { get; set; }
        /// <summary>
        /// Only comments updated at or after this time are returned.///                                              This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
        /// </summary>
        [RequestType("query")]
        [JsonProperty("since")]
        public string Since { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }
        /// <summary>
        /// 排序顺序: asc(default),desc
        /// </summary>
        [RequestType("query")]
        [JsonProperty("order")]
        public string Order { get; set; }

    }
}