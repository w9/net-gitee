using GiteeOpenSdk.Common;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.Request.Issues
{
    /// <summary>
    /// 获取授权用户的所有Issues
    /// </summary>
    [RequestMethod("GET")]
    public partial class GetV5UserIssuesRequestModel : GiteeRequestModel
    {
        /// <summary>
        /// 用户授权码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned
        /// </summary>
        [RequestType("query")]
        [JsonProperty("filter")]
        public string Filter { get; set; }
        /// <summary>
        /// Issue的状态: open（开启的）, progressing(进行中), closed（关闭的）, rejected（拒绝的）。 默认: open
        /// </summary>
        [RequestType("query")]
        [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// 用逗号分开的标签。如: bug,performance
        /// </summary>
        [RequestType("query")]
        [JsonProperty("labels")]
        public string Labels { get; set; }
        /// <summary>
        /// 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
        /// </summary>
        [RequestType("query")]
        [JsonProperty("sort")]
        public string Sort { get; set; }
        /// <summary>
        /// 排序方式: 升序(asc)，降序(desc)。默认: desc
        /// </summary>
        [RequestType("query")]
        [JsonProperty("direction")]
        public string Direction { get; set; }
        /// <summary>
        /// 起始的更新时间，要求时间格式为 ISO 8601
        /// </summary>
        [RequestType("query")]
        [JsonProperty("since")]
        public string Since { get; set; }
        /// <summary>
        /// 当前的页码
        /// </summary>
        [RequestType("query")]
        [JsonProperty("page")]
        public int? Page { get; set; }
        /// <summary>
        /// 每页的数量，最大为 100
        /// </summary>
        [RequestType("query")]
        [JsonProperty("per_page")]
        public int? PerPage { get; set; }
        /// <summary>
        /// 计划开始日期，格式：20181006T173008+80-20181007T173008+80（区间），或者 -20181007T173008+80（小于20181007T173008+80），或者 20181006T173008+80-（大于20181006T173008+80），要求时间格式为20181006T173008+80
        /// </summary>
        [RequestType("query")]
        [JsonProperty("schedule")]
        public string Schedule { get; set; }
        /// <summary>
        /// 计划截止日期，格式同上
        /// </summary>
        [RequestType("query")]
        [JsonProperty("deadline")]
        public string Deadline { get; set; }
        /// <summary>
        /// 任务创建时间，格式同上
        /// </summary>
        [RequestType("query")]
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }
        /// <summary>
        /// 任务完成时间，即任务最后一次转为已完成状态的时间点。格式同上
        /// </summary>
        [RequestType("query")]
        [JsonProperty("finished_at")]
        public string FinishedAt { get; set; }

    }
}