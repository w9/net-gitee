﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.BaseModels
{
    /// <summary>
    /// 仓库模型
    /// </summary>
    public class RespositoryModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("full_name")]
        public string FullName { get; set; }
        [JsonProperty("human_name")]
        public string HumanName { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("namespace")]
        public NameSpaceModel NameSpace { get; set; }
        [JsonProperty("path")]
        public string Path { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("owner")]
        public UserModel Owner { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("private")]
        public bool Private { get; set; }
        [JsonProperty("public")]
        public bool Public { get; set; }
        [JsonProperty("internal")]
        public bool Internal { get; set; }
        [JsonProperty("fork")]
        public bool Fork { get; set; }
        [JsonProperty("html_url")]
        public string HtmlUrl { get; set; }
        [JsonProperty("ssh_url")]
        public string SshUrl { get; set; }
        [JsonProperty("forks_url")]
        public string ForksUrl { get; set; }
        [JsonProperty("keys_url")]
        public string KeysUrl { get; set; }
        [JsonProperty("collaborators_url")]
        public string CollaboratorsUrl { get; set; }
        [JsonProperty("hooks_url")]
        public string HooksUrl { get; set; }
        [JsonProperty("branches_url")]
        public string BranchesUrl { get; set; }
        [JsonProperty("tags_url")]
        public string TagsUrl { get; set; }
        [JsonProperty("blobs_url")]
        public string BlobsUrl { get; set; }
        [JsonProperty("stargazers_url")]
        public string StargazersUrl { get; set; }
        [JsonProperty("contributors_url")]
        public string ContributorsUrl { get; set; }
        [JsonProperty("commits_url")]
        public string CommitsUrl { get; set; }
        [JsonProperty("comments_url")]
        public string CommentsUrl { get; set; }
        [JsonProperty("issue_comment_url")]
        public string IssueCommentUrl { get; set; }
        [JsonProperty("issues_url")]
        public string IssuesUrl { get; set; }
        [JsonProperty("pulls_url")]
        public string PullsUrl { get; set; }
        [JsonProperty("milestones_url")]
        public string MilestonesUrl { get; set; }
        [JsonProperty("notifications_url")]
        public string NotificationsUrl { get; set; }
        [JsonProperty("labels_url")]
        public string LabelsUrl { get; set; }
        [JsonProperty("releases_url")]
        public string ReleasesUrl { get; set; }
        [JsonProperty("recommend")]
        public bool Recommend { get; set; }
        [JsonProperty("homepage")]
        public string Homepage { get; set; }
        [JsonProperty("language")]
        public string Language { get; set; }
        [JsonProperty("forks_count")]
        public int ForksCount { get; set; }
        [JsonProperty("stargazers_count")]
        public int StargazersCount { get; set; }
        [JsonProperty("watchers_count")]
        public int WatchersCount { get; set; }
        [JsonProperty("default_branch")]
        public string DefaultBranch { get; set; }
        [JsonProperty("open_issues_count")]
        public int OpenIssuesCount { get; set; }
        [JsonProperty("has_issues")]
        public bool HasIssues { get; set; }
        [JsonProperty("has_wiki")]
        public bool HasWiki { get; set; }
        [JsonProperty("issue_comment")]
        public bool IssueComment { get; set; }
        [JsonProperty("can_comment")]
        public bool CanComment { get; set; }
        [JsonProperty("pull_requests_enabled")]
        public bool PullRequestsEnabled { get; set; }
        [JsonProperty("has_page")]
        public bool HasPage { get; set; }
        [JsonProperty("license")]
        public string License { get; set; }
        [JsonProperty("outsourced")]
        public bool Outsourced { get; set; }
        [JsonProperty("project_creator")]
        public string ProjectCreator { get; set; }
        [JsonProperty("members")]
        public List<string> Members { get; set; }
        [JsonProperty("pushed_at")]
        public DateTimeOffset PushedAt { get; set; }
        [JsonProperty("created_at")]
        public DateTimeOffset CreatedAt { get; set; }
        [JsonProperty("updated_at")]
        public DateTimeOffset UpdatedAt { get; set; }
        [JsonProperty("parent")]
        public RespositoryModel Parent { get; set; }
        //[JsonProperty("paas")] todo:查清这个字段的含义以及属性
        //public ? Paas { get; set; }
        [JsonProperty("assignees_number")]
        public int AssigneesNumber { get; set; }
        [JsonProperty("testers_number")]
        public int TestersNumber { get; set; }
        [JsonProperty("assignees")]
        public List<UserModel> Assigness { get; set; }
        [JsonProperty("testers")]
        public List<UserModel> Testers { get; set; }

    }
}
