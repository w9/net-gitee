﻿using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.BaseModels
{
    public class BranchModel : BaseBranchModel
    {
        [JsonProperty("_links")]
        public Links Links { get; set; }
    }
    public class BaseBranchModel : GiteeResponseModel
    {
        [JsonProperty("commit")]
        public CommitModel Commit { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("protected")]
        public string Protected { get; set; }
        [JsonProperty("protection_url")]
        public string ProtectionUrl { get; set; }
    }
}
