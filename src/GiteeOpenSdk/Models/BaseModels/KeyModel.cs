﻿using System;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.BaseModels
{
    /// <summary>
    /// key模型
    /// </summary>
    public class KeyModel : GiteeResponseModel
    {
        [JsonProperty("created_at")]
        public DateTimeOffset CreatedAt { get; set; }
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("key")]
        public string Key { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
