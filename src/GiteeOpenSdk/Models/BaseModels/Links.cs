﻿using System;
using Newtonsoft.Json;

namespace GiteeOpenSdk.Models.BaseModels
{
    public class Links
    {
        [JsonProperty("html")]
        public string Html { get; set; }
        [JsonProperty("self")]
        public string Self { get; set; }
    }
}
