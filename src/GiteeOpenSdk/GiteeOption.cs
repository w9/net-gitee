﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiteeOpenSdk
{
    public class GiteeOption
    {
        /// <summary>
        /// 应用ID
        /// </summary>
        public string ClientId { get; set; }
        /// <summary>
        /// 应用密钥
        /// </summary>
        public string ClientSecret { get; set; }
        /// <summary>
        /// 回调Url
        /// </summary>
        public string RedirectUri { get; set; }
        /// <summary>
        /// 授权码
        /// </summary>
        public string AccessToken { get; set; }
    }
}
