﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiteeOpenSdk.Common
{
    /// <summary>
    /// 参数类型 query,path,fromData
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class RequestTypeAttribute : Attribute
    {
        public RequestTypeAttribute(string RequestType)
        {
            this.RequestType = RequestType;
        }

        private string _RequestType;
        /// <summary>
        /// 参数类型
        /// </summary>
        public string RequestType
        {
            get { return _RequestType; }
            set { _RequestType = value; }
        }
    }
}
