﻿using GiteeOpenSdk.Models;
using GiteeOpenSdk.Models.Request.Issues;
using GiteeOpenSdk.Models.Response.Issues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiteeOpenSdk.Services
{
    /// <summary>
    /// 任务、Issue接口
    /// </summary>
    public class IssuesApi : GiteeCommonApi
    {
        public IssuesApi() { }
        public IssuesApi(string clientId, string clientSecret, string accessToken) : base(clientId, clientSecret, accessToken) { }
        /// <summary>
        /// 获取企业的某个Issue
        /// </summary>
        public async Task<ResponseModel<GetV5EnterprisesEnterpriseIssuesNumberResponseModel>> GetV5EnterprisesEnterpriseIssuesNumber(GetV5EnterprisesEnterpriseIssuesNumberRequestModel model)
        {
            var result = await ExecuteAsync<GetV5EnterprisesEnterpriseIssuesNumberRequestModel, GetV5EnterprisesEnterpriseIssuesNumberResponseModel>("enterprises/{enterprise}/issues/{number}", model);
            return result;
        }
        /// <summary>
        /// 更新企业的某个Issue
        /// </summary>
        public async Task<ResponseModel<PatchV5EnterprisesEnterpriseIssuesNumberResponseModel>> PatchV5EnterprisesEnterpriseIssuesNumber(PatchV5EnterprisesEnterpriseIssuesNumberRequestModel model)
        {
            var result = await ExecuteAsync<PatchV5EnterprisesEnterpriseIssuesNumberRequestModel, PatchV5EnterprisesEnterpriseIssuesNumberResponseModel>("enterprises/{enterprise}/issues/{number}", model);
            return result;
        }
    }
}
