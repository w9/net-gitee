﻿        /// <summary>
        /// {{summary}}
        /// </summary>
        [RequestType("{{requestType}}")]
        [JsonProperty("{{jsonProperty}}")]
        public {{propertyType}} {{propertyName}} { get; set; }
