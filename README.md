# net-gitee
[![star](https://gitee.com/w9/net-gitee/badge/star.svg?theme=dark)](https://gitee.com/w9/net-gitee/stargazers)
[![fork](https://gitee.com/w9/net-gitee/badge/fork.svg?theme=dark)](https://gitee.com/w9/net-gitee/members)
[![GitHub license](https://img.shields.io/badge/license-Apache2-yellow)](https://gitee.com/w9/net-gitee/blob/master/LICENSE)
[![Nuget](https://img.shields.io/nuget/v/Leo.GiteeOpenSdk)](https://www.nuget.org/packages/Leo.GiteeOpenSdk/)

### 介绍
net-gitee is the .Net SDK of Gitee OpenAPI.

[Gitee API地址（Developor）](https://gitee.com/api/v5/swagger)

[Gitee API地址（Enterprise）](https://gitee.com/api/v8/swagger)

### 实现功能

- [ ] socket
- [x] CodeGen
- [ ] 模型搬运（ :sweat: 由于Gitee文档存在一定问题，加上本人时间不足，可能比较拖沓。）
- [ ] 全面的使用文档
- [ ] Web调用示例
- [ ] 全面的说明文档
- [x] webhook模型
- [x] webhook签名验证
- [x] Api调用
- [x] 授权设计
- [x] 模型设计
- [x] 框架设计

### 软件架构
- CodeGen（代码生成器-目前主要生成请求模型）
  - Template（模板）
    - Class
    - Property
  - GiteeApiV5.json（Gitee的SwaggerJson文件：[入口](https://gitee.com/api/v5/doc_json)）
  - GiteeApiV8.json（Gitee的SwaggerJson文件：[入口](https://gitee.com/api/v8/doc_json?oauth_type=enterprise)）
  - Swagger-V2 Model
- Console（控制台程序-主要测试一些接口还原和实现）
- GiteeOpenSdk（核心程序）
  - Common（用到的简单工具包）
  - Models（Api模型-从CodeGen导入，不建议修改，请使用`partial`扩展）
    - Base（重用的一部分模型）
    - Request（请求模型）
    - Response（返回模型）
    - 其他（基础模型和授权模型）
  - Services（核心服务）
  - WebHook（WebHook钩子支持）
    - Models（钩子模型）
    - SignVerification（验证签名）
- Sample（Web实践示例代码） **（待更新）** 
  - Sdk调用样例
  - WebHook样例
- SwaggerCodegen（使用SwaggerCodegen自动生成的.Net Core SDK）

### 最佳实践（后续会逐渐开源）
- 开始任务自动创建分支并关联（已实现，完善后开源）
  - 解析WebHook(issue_state_change)
  - 查询分支
  - 创建分支
  - 任务关联分支
- 任务审核自动创建PR（规划中）
- PR审核自动关闭关联任务（现在要在pr内容中手动输入`#IssueID`来关联任务）（规划中）
- 里程碑提醒&任务到期提醒（轮询）

### 安装教程（待更新）

### 使用说明（待更新）
### 注意
- 模型使用`partial`部分类（可自扩展）
- 模型命名规范：基于`swagger.json`的`operationId`字段
### 🥗 环境要求

- Visual Studio 2019 16.8 +（或mac版）
- .NET 5 SDK +
- .Net Standard 2.1 +(待更新)

### 🍻 贡献代码

`net-gitee` 遵循 `Apache-2.0` 开源协议，欢迎大家提交 `PR` 或 `Issue`。

如果要为项目做出贡献，请查看 [贡献指南](https://dotnetchina.gitee.io/furion/docs/contribute)。